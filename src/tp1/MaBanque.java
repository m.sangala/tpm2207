package tp1;

public class MaBanque {

	public static void main(String[] args) {
		
		// Cr�ation d'un compte c1
		Compte c1;
		c1 = new Compte(1);
		
		// Test du compte c1
		System.out.println("Compte N�1");
		System.out.println("Decouvert autoris� : " + c1.getDecouvert()); // Affichage du decouvert du compte c1
		System.out.println("Ajout d'un decouvert de 100");
		c1.setDecouvert(100); // Attribution d'un entier au decouvert au compte c1
		System.out.println("Decouvert autoris� : " + c1.getDecouvert());
		System.out.println("Voici votre solde : " + c1.getSolde());
		
		// Cr�ation d'un compte c2
		Compte c2;
		c2 = new Compte(2);
		
		// Test du compte c2
		System.out.println("Compte N�2");
		System.out.println("La solde de c2 est de " + c2.getSolde()); // Affichage de la solde
		System.out.println("Versement a faire : 1000");
		c2.Depot(1000); // Depot de 1000 sur la solde
		System.out.println("Le montant de la solde est a present de : " + c2.getSolde()); // Affichage de la solde
		System.out.println("Retrait de : 600");
		c2.Retrait(600); // Retrait de 600
		System.out.println("Retrait de : 700");
		c2.Retrait(700);
		System.out.println("Ajout d'un decouvert de 500");
		c2.setDecouvert(500); // Ajout d'un decouvert de 500
		System.out.println("Decouvert de : " + c2.getDecouvert()); // Affichage du decouvert
		System.out.println("Retrait de : 700");
		c2.Retrait(700); // retrait de 700
		
	}

}
