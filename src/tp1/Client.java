package tp1;

public class Client {
	
	// Attribut
	public String nom;
	public String prenom;
	Compte CompteCourant;
	
	// Constructeur qui permet d'afficher les informations des clients
	public Client(String nom, String prenom, Compte compte){
			this.nom = nom;
			this.prenom = prenom;
			this.CompteCourant = compte;		
	}
	// Accesseur pour obtenir le nom du client
	public String getNom() {
		return nom;
	}
	// Accesseur pour obtenir le prenom du client
	public String getPrenom() {
		return prenom;
	}
	
}
