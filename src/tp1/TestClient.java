package tp1;

public class TestClient {

	public static void main(String[] args) {
			
		// Cr�ation d'un client
		Client kanto; // Nom de la variable client
		Compte compte1 = new Compte(1234);
		kanto = new Client("Randrianasolo", "Kanto",compte1); // Ajout d'information sur le client Kanto
		System.out.println(kanto.getNom()); // Synchronisation entre le client et ses informations
		System.out.println(kanto.getPrenom());

	}

}
