package tp1;

public class Compte {
	
	// Cr�ation des attribut priv�
	private int numero;
	public double solde;
	private double decouvert;

	// Constructeur Compte qui initialise a 0 la solde et le decouvert
	public Compte (int numero) {
		this.numero = numero;
	}
	
	// Constructeur qui fixe le decouvert
	void setDecouvert(double montant) {
		this.decouvert = montant; // Permet d'ajouter la nouvelle valeur du d�couvert a la variable d�couvert
	}
	
	// Constructeur pour recuperer la valeur du decouvert
	double getDecouvert() {
		return decouvert;
	}
	
	int getNumero() {
		return numero;
	}
	
	double getSolde() {
		return solde;
	}
	
	// Constructeur pour afficher la solde
	void afficherSolde() {
		System.out.println("Votre solde est de : " + solde);
	}
	
	// Constructeur pour le d�pot d'argent sur le compte
	void Depot( double montant) {
		this.solde = this.solde + montant; // Ajout d'un montant a la solde qui se trouve deja sur le compte
	}
	
	// Constructeur pour le retrait d'argent 
	void Retrait(double montant) {

		// Gestion des retrait autoris�
		if (montant <= this.solde + decouvert){
			System.out.println("Retrait autoris�");
			System.out.println("Retrait de -" + montant); // Affichage du montant retir�
			this.solde = this.solde - montant;
		}
		else {
			System.out.println("Retrait refus�");
		}
		
	}
}
