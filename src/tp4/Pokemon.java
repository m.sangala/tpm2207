package tp4;

public class Pokemon {
	
	// Attributs
	private int energie, maxEnergie; // Attribut qui fixe l'energie et la limite de l'energie du pokemon
	public String nom;
	public int cycle;
	private int puissance;
	int perte;
	
	// Constucteur
	public Pokemon(String nom) {
		this.nom = nom;
		maxEnergie = 50 + (int) (Math.random() * ( (90 - 50) + 1 ) ); // Operation math�matique permettant de choisir un nombre random
		energie = 30 + (int) (Math.random() * ( (maxEnergie - 30) + 1 ) );
		puissance = 3 + (int) (Math.random() * ( (10 - 3) + 1 ) );
	}
	
	// Accesseurs
	String getNom() {
		return nom;
	}
	
	int getEnergie() {
		return energie;
	}
	
	int getPuissance() {
		return puissance;
	}

	// methode
	void sePresenter() {
		System.out.println("Je suis " + nom + ", j'ai " + energie + " unit�s d'energies " + "( " + maxEnergie + " ) et une puissance de " + puissance);
	}
	
	void manger() {
		if (energie < maxEnergie) {
			int ajout = 10 + (int) (Math.random() * ( (30 - 10) + 1 ) );
			energie = energie + ajout;
			System.out.println(nom + " prends " + ajout + " PV ");
			if (energie > maxEnergie) {
				int jeter = energie - maxEnergie;
				energie = maxEnergie;
				System.out.println("Je n'ai plus faim, jai jeter " + jeter + " de nourriture");
				System.out.println(nom + " possede " + energie + " PV ");
			}
		}
		
		else {
			System.out.println("Je n'ai pas faim");
			System.out.println(nom + " possede " + energie + " PV ");
		}
	}
	
	void vivre() {
		if (energie > 0) {
			int retrait = 20 + (int) (Math.random() * ( (40 - 20) + 1 ) );
			energie = energie - retrait;
			System.out.println(nom + " perds " + retrait + " PV ");
			if (energie < 0) {
				energie = 0;
				System.out.println(nom + " possede " + energie + " PV ");
				System.out.println(nom + " est mort !");
			}
			else {
				System.out.println(nom + " possede " + energie + " PV ");
			}
			
			}
		}
	
	boolean isAlive() {
		if (energie > 0) {
			return true;
		}
		
		else {
			return false;
		}
	}
	
	void perteEnergie(int perte) {
		this.perte = perte;
		
		if (energie < ((maxEnergie * 25)/100)) {
			perte = perte * 2;
		}
		if (perte > energie) {
			energie = 0;
		}
		else {
		energie -= perte;
		}

	}
	
	void attaquer(Pokemon adversaire) {
		
		if (energie < ((maxEnergie * 25)/100)) {
			puissance = (puissance * 2);
		}
		adversaire.perteEnergie(getPuissance());
		int fatigue = ((0 + (int) (Math.random() * (1-0) + 1)));
		puissance = puissance - fatigue;
		
		if (puissance < 0) {
			puissance = 1;

		System.out.println(adversaire.nom + " a perdu " + perte + " PV");
	}
	
	}
	
}
	
	
