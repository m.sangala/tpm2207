package tp4;

public class Combat {

	public static void main(String[] args) {
		// Attribut
		int rounds = 0;
		
		// Creation du premier combatant
		Pokemon pikatchu;
		pikatchu = new Pokemon("pikatchu");
		pikatchu.sePresenter();
		
		// Creation du deuxi�me combattant
		Pokemon raichu;
		raichu = new Pokemon("raichu");
		raichu.sePresenter();
		
		
		while (pikatchu.getEnergie() > 0 && raichu.getEnergie() > 0) {
			
			rounds += 1;
			pikatchu.attaquer(raichu);
			raichu.attaquer(pikatchu);
			System.out.println("Round " + rounds + " - " + pikatchu.getNom() + " : " + " energie = " + pikatchu.getEnergie() + " " + " puissance = " + pikatchu.getPuissance() + " " + raichu.getNom() + " : " + " energie = " + raichu.getEnergie() + " " +  " puissance = " + raichu.getPuissance());
		}
		if (pikatchu.getEnergie() > 0) {
			System.out.println(pikatchu.getNom() + " gagne en " + rounds + " rounds");
		}
		if (raichu.getEnergie() > 0) {
			System.out.println(raichu.getNom() + " gagne en " + rounds + " rounds !");
		}
		if (pikatchu.getEnergie() == 0 && raichu.getEnergie() == 0) {
			System.out.println("Match nul !");

			
		}

	}

}
