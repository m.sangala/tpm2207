package tp4;



public class TestPokemon {

	public static void main(String[] args) {
		
		// Attributs
		int cycle = 0;
		
		// Creation du Pokemon Salameche
		Pokemon Salameche;
		Salameche = new Pokemon("Salameche");
		Salameche.sePresenter();
		while (Salameche.isAlive() == true) {
			Salameche.manger();
			Salameche.vivre();	
			Salameche.isAlive();
			cycle += 1;
		}
		System.out.println(Salameche.getNom() + " a v�cu " + cycle + " cycle");
	}

}
