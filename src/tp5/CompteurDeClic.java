package tp5;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.*;


public class CompteurDeClic extends JFrame implements ActionListener{ // ActionListener permet d'ecouter les entr�e clavier ou souris
	
	// Attributs ------------
	JButton click;
	JLabel nbClick;
	int Clicked = 0;
	
	
	// Constructeur ---------
	public CompteurDeClic() {
		super();
		this.setTitle("Compteur de Clics !");
		this.setSize(200,100);
		this.setLocation(20,20);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 
		
		
		
		
		// Declaration ------
		click = new JButton("Cliquez moi !"); // Creation du bouton
		nbClick = new JLabel("Vous avec cliqu� " + Clicked + " fois");
		

		
		// Container --------
		Container p = getContentPane();
		p.setLayout(new FlowLayout());
		
		p.add(click);

		
		click.addActionListener(this);
		p.add(nbClick);
		
		
		System.out.println("La fen�tre est cr��e !");
		this.setVisible(true);
	}
	
	public void actionPerformed(ActionEvent e) {
		Clicked ++;
		nbClick.setText("Vous avec cliqu� " + Clicked + " fois");
		System.out.println("Une action a �t� d�tect�e");  
		
	} 
	
	
	public static void main(String[] args) {
		CompteurDeClic compteur = new CompteurDeClic ();  

	} 


}