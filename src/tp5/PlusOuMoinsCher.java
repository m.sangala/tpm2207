package tp5;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;



public class PlusOuMoinsCher extends JFrame implements ActionListener {
	
	// Attributs -------------
	JButton verifie;					// Un bouton
	JLabel monLabel1, monLabel2;		// Deux zone de texte
	JTextField monTextField;			// Une zone de texte editable
	int prix;							// Variable qui servira pour avoir une valeur entre 0 et 100
	int i = 0;							// Compteur qui servira a compter le nombres de tentative
	
	// Constructeurs ---------
	public PlusOuMoinsCher() {
		
		super();
		this.setTitle("Plus cher ou moins cher");					   // Titre de la fenetre
		this.setSize(550,150);										   // Taille fenetre
		this.setLocation(300,150);									   // Zone d'affichage de la fenetre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
		
		// Declarations -------
		verifie = new JButton("V�rifier !");					        // Cree un bouton avec pour nom verifie !
		monLabel1 = new JLabel("  Proposez un nombre : ");		        // Premier zone de texte 
		monLabel2 = new JLabel("  La r�ponse");					        // Deuxieme zone de texte
		monTextField = new JTextField();						        // Zone de texte modifiable (ou l'on rentrera notre valeur)
		prix = (int)(Math.random() * 100 +1);					        // Valeur aleatoire, il faudra le trouver !
		
		Container boite = getContentPane();				                // Creation du container, la ou l'on place les different objet;
		
		boite.setLayout(new GridLayout(2,2));			                // Il y aura 2 objets par ligne, les objet prendrons toutes la places disponibles sur la fenetre
		boite.add(monLabel1);							                // Place ma zone de texte dans le container
		boite.add(monTextField);						                // Place ma zone de texte modifiable dans le container
		boite.add(verifie);								                // Place mon bouton dans le container
		boite.add(monLabel2);							                // Place ma 2e zone de texte dans le container
		
		verifie.addActionListener(this);		                        // Permet de reconnaitre l'action lorsqu'on appuie sur le bouton verifie !
		
		System.out.println("La fen�tre a ete creee ! ");
		this.setVisible(true); 					                        // Pour que tout les element sois bien visible
	}
		
	public void actionPerformed(ActionEvent e) { 						// Liste d'action a effectuer lorsqu'on appuye sur le bouton verifie !
		
		i++;				                                            // Le compteur augmente de 1 a chaque clic		
		if (i >= 8) {													// Si i est superieur ou egal a 8
			monLabel2.setText("  Vous avez epuise vos essais");			//	Plus d'essais disponible, le jeu s'arrete
		}
			
		else {															// Sinon le jeu continue
		System.out.println("Voici le nombre a devine : "+ prix);		// Affiche la valeur a trouver dans la console
		String valeur = monTextField.getText();							// Stocke la valeur dans la zone de texte modifiable (JTextField) dans une variable
		int proposition = Integer.parseInt(valeur);						// Permet de convertir la chaine valeur en un entier


			if (proposition < prix) 
				monLabel2.setText("  Plus cher ! il vous reste " + (8-i) + " tentative(s) !");  // Si le nombre inserer dans monTextField est inferieur au nombre a devine, alors le jeu continue et on perd une tentative
			
			else if (proposition > prix) 
				monLabel2.setText("  Moin cher ! il vous reste " + (8-i) + " tentative(s) !");
			
			else
				monLabel2.setText("  Vous avez devine ! Au bout de " + i + " tentatives !");
		}
		
		System.out.println("Une action a �t� d�tect�e");  
		
	} 	

	public static void main(String[] args) {
		PlusOuMoinsCher panneau = new PlusOuMoinsCher();
		
	}
	
}
