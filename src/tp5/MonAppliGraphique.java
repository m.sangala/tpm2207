package tp5; 

import javax.swing.JFrame; 
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.BorderLayout;


public class MonAppliGraphique extends JFrame{
	
	// Attributs -----------
	JButton b,b1,b2,b3,b4;
	JLabel monLabel;
	JTextField monTextField;

	// Constructeurs -------
	public MonAppliGraphique () {
		super();
		this.setTitle("Ma premi�re application");
		this.setSize(400,200);
		this.setLocation(20,20);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		
		
		// Declaration ------
		
		b = new JButton("Bouton 0"); // Appel au package JButton
		b1 = new JButton("Bouton 1");
		b2 = new JButton("Bouton 2");
		b3 = new JButton("Bouton 3");
		b4 = new JButton("Bouton 4");		
		monLabel = new JLabel("Je suis un JLabel");	
		monTextField = new JTextField(" Je suis un JTextField ");
		
		
		// Container ---------
		
		Container panneau = getContentPane();
		//panneau.setLayout(new FlowLayout()); // Le bouton ne prend plus toute la fenetre.
		panneau.setLayout(new GridLayout(3,2)); // Le bouton prend toute la fenetre
		//panneau.setLayout(new BorderLayout());
		
		panneau.add(b, BorderLayout.NORTH);
		panneau.add(b1, BorderLayout.SOUTH);
		panneau.add(b2, BorderLayout.EAST);
		panneau.add(b3, BorderLayout.WEST);
		panneau.add(b4); // Valeur par defaut BorderLayout.CENTER
		//panneau.add(monLabel);
		//panneau.add(monTextField);
		
	
		System.out.println("La fen�tre � �t� cr��e !");
		this.setVisible(true);  // Toujours plac� a la fin du constructeur
	}
	
	
	

	// Main -----------------
	
	public static void main(String[] args) {
		MonAppliGraphique app = new MonAppliGraphique ();  

	} 	
	
} 
