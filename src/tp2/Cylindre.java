package tp2;

public class Cylindre extends Cercle {
	
		//Attributs
		private double hauteur;


		//Constructeurs
		public Cylindre() {
			hauteur= 1.0;
		}

		Cylindre(double h, double r, String couleur, boolean coloriage){
			super();

		}


		//Accesseurs
		double getHauteur() {
			return hauteur;
		}

		void setHauteur(double h) {
			hauteur= h;
		}

		//M�thodes
		String seDecrire(){
			return "Un cercle de rayon"+" "+getRayon()+" de hauteur"+" "+hauteur+" "+"et est issue d'une Forme de couleur"+" "+getCouleur()+" "+"et de coloriage"+" "+isColoriage(); 
		}
		double calculerVolume() {
			return Math.PI*hauteur*getRayon()*getRayon();
		}


}
