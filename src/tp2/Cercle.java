package tp2;

public class Cercle extends Forme {
	
	// Attribut
	private double rayon;
	
	// Constructeur
	Cercle (){
		super();
		rayon = 1.0; 
	}
	
	Cercle(double r){
		r= 1.0;
	}

	Cercle(double r, String couleur, boolean coloriage){
		super();

	}

	
	// Accesseurs
	void setRayon(double r) { // Accesseur qui pose un rayon
		rayon= r;
	}

	double getRayon() { // Accesseur pour recuperer le rayon
		return rayon;
	}
	
	// Methode
	String seDecrire(){ // Methode pour decrire un cercle
		return "Un cercle de rayon"+" "+rayon+" "+"est issue d'une Forme de couleur"+" "+getCouleur()+" "+"et de coloriage"+" "+isColoriage(); 
	}

	double calculerAire() { // Methode pour calculer l'aire du cercle
		return Math.PI*rayon*rayon;
	}
	
	double calculerPerimetre() { // Methode pour calculer le perimetre d'un cercle
		return Math.PI*2*rayon;
	}



}
