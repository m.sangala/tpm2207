package tp2;

public class Forme {
	
	// Attribut
	String couleur;
	boolean coloriage;
	
	// Constructeur
	public Forme() { // Constructeur qui pose la couleur en orange et le coloriage en true
		couleur = "orange";
		coloriage = true;
	}
	
	public Forme (String c, boolean r) { // Constructeur qui permet d'initialiser la couleur et le coloriage
		this.coloriage = r;
		this.couleur = c;
	}
	
	//Accesseurs
	String getCouleur() { // Accesseur permettant de recuperer la couleur
		return couleur;
	}
	
	void setCouleur(String c) { // Accesseur permettant de poser une couleur
		this.couleur = c;
	}
	
	boolean isColoriage() { // Accesseur permettant de recuperer le coloriage
		return coloriage;
	}
	
	void setColoriage( boolean b) { // Accesseur permettant de poser le coloriage
		this.coloriage = b;
	}

	//Methode
	String seDecrire() { // Simplifie l'affichage des infos d'une forme
		return "Une forme de couleur"+" "+couleur+" "+ "et de coloriage"+" "+coloriage;
	}
}
