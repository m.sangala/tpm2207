package tp2;

public class TestForme {

	public static void main(String[] args) {
		
		// Exercice 1
		Forme f1,f2;// Creation des instance f1 et f2 de class Forme
		f1 = new Forme();
		f2 = new Forme("vert", false);
		
		System.out.println("f1:"+" "+f1.getCouleur()+" "+f1.isColoriage());
		System.out.println("f2:"+" "+f2.getCouleur()+" "+ f2.isColoriage());

		f1.setCouleur("rouge");
		f1.setColoriage(false);

		System.out.println("f1:"+" "+f1.getCouleur()+" "+f1.isColoriage());

		System.out.println("f1 est:"+" "+f1.seDecrire());
		System.out.println("f2 est:"+" "+f2.seDecrire());

		// Exercice 2
		// Creation du cercle c1
		Cercle c1;
		c1= new Cercle();
		System.out.println("le cercle c1  pour rayon"+" "+c1.getRayon()); // Affichage du rayon du cercle
		System.out.println("c1 est:"+" "+c1.seDecrire()); // utilisation de sedecrire sur le cerlce c1 pour le decrire

		// Creation du cercle c2
		Cercle c2;
		c2= new Cercle();
		c2.setRayon(2.5);
		System.out.println("Le rayon du cercle c2 est:"+" "+c2.getRayon());

		// Creation du cercle c3
		// Changement sur le cercle c3
		Cercle c3;
		c3= new Cercle();
		c3.setCouleur("jaune");
		c3.setColoriage(false);
		c3.setRayon(3.2);

		// Affichage des changements du cercle c3
		System.out.println("Le cercle c3:"+" "+c3.getCouleur()+" "+c3.getRayon()+" "+c3.isColoriage());
		
		// Calcul des aires et des perimètres
		System.out.println("aire de c3: "+c3.calculerAire());
		System.out.println("aire de c2: "+c2.calculerAire());
		System.out.println("perimtre de c3: "+c3.calculerPerimetre());
		System.out.println("perimtre de c2: "+c2.calculerPerimetre());

		// Exercice 3
		
		// Creation des cylindres 1 et 2
		Cylindre cy1, cy2;
		cy1= new Cylindre();
		cy2= new Cylindre();
		
		// Affichage des caracteristique du cylindre c1
		System.out.println("Le cylindre cy1:"+" "+cy1.getCouleur()+" "+cy1.getRayon()+" "+cy1.isColoriage());
		
		// Changement de caracteristique du cylindre c2
		cy2.setCouleur("bleu");
		cy2.setColoriage(true);
		cy2.setRayon(1.3);
		cy2.setHauteur(4.2);
		
		// Affichage des caracteristique du cylindre c2
		System.out.println("cy2 est: "+cy2.seDecrire());
		
		// Calcule du volume des cylindres
		System.out.println("volume cy2: "+cy2.calculerVolume());
		System.out.println("volume cy1: "+cy1.calculerVolume());
	}

}
