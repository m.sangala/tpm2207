package tp6;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.*;

public class PanneauServeur extends JFrame implements ActionListener{

	// Attributs ------------
	Container ecran;
	JButton exit;
	JTextArea texte;
	JScrollPane scrolleur;

	// Constructeur
	// Caracteristique de la fenetre ------
	public PanneauServeur() {
		super();
		setTitle("Serveur - Panneau d'affichage");
		setSize(400,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


		// D�clarations ------
		exit = new JButton("Quitter"); // Bouton quitter
		texte = new JTextArea("Ce panneau est actif\n"); 		// Zone de texte modifiable
		scrolleur = new JScrollPane(texte); 					// Un scrolleur pour la zone de texte ( donc affiche aussi le text ) 


		// Le serveur -------




		// Ce qui sera affiche dans la fenetre -----
		Container ecran = getContentPane(); 					// Pour avoir un pane
		//ecran.setLayout(new FlowLayout());
		ecran.setLayout(new BorderLayout()); 					// Pour que les Content prennent toute la place dispo
		exit.addActionListener((ActionListener) this);			// Permet au bouton exit de  reagir a une action lorsqu'on clic dessus
		ecran.add(exit, BorderLayout.SOUTH); 					// Ajouter le bouton exit en Bas
		//ecran.add(texte); 									// Ajouter la zone de texte par d�faut au centre (ne sert plus car apparait dans le JScroll
		ecran.add(scrolleur); 									// Ajoute un scrolleur
		//texte.append("hello\n");								// Permet d'ajouter du texte dans le JTextArea
		this.setVisible(true); 									// Mettre un setVisible ici permet d'afficher le panneau avant l'execution du serveur

		
		try {
			ServerSocket monServerSocket;						
			Socket monSocketClient;
			BufferedReader monBufferedReader;


			monServerSocket = new ServerSocket(8888);			// Nouveau Serveur ouvert dans le port 8888
			texte.append("Serveur D�marr� ! Veuillez vous connecter avec le port 8888.\n"); // Affiche un texte dans la fen�tre si le serveur a bien demarrer
			System.out.println("ServerSocket : " + monServerSocket); // Affiche l'adresse et le port de connexion dans la console
			 //monServerSocket.close();

			monSocketClient = monServerSocket.accept();			// Permet au client de se connecter au serveur (via telnet)
			texte.append("Le client s'est connect�" + "\n");	// Affiche un message dans la fenetre si le client s'est bien connecter au serveur
			//monSocketClient.close();

			monBufferedReader = new BufferedReader(new InputStreamReader(monSocketClient.getInputStream()));  // Permet de stocker les donnees envoyer par le client
			String ligne;
			while ((ligne = monBufferedReader.readLine()) != null) { // B	oucle permettant au client d'envoyer de la donnee sans que la fenetre se ferme
				texte.append("Message : " + ligne + "\n");		// Affiche le message envoyer par le client en sautant de ligne
			}
				

			monServerSocket.close();							// Ferme le serveur
			monSocketClient.close();							// Ferme le client Socket
		}
		catch (Exception e) {									// Lors d'une erreur
			texte.append("Erreur dans le traitement de la connexion"); // Affiche ce message dans la fenetre en cas d'erreur
			System.out.print("une exception a �t� captur�e" + "\n" ); // 
			e.printStackTrace();
		}




		System.out.println("La fenetre a ete cree !"); 
		this.setVisible(true); 
	}

	public void actionPerformed(ActionEvent e) { 					// Le bouton exit reagit au action suivant
		System.exit(-1); 											// Ferme la fenetre

	}

	public static void main(String[] args) {

		PanneauServeur panneau = new PanneauServeur();

	}

}