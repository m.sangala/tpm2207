package tp6;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;

import javax.swing.*;

public class PanneauClient extends JFrame implements ActionListener { 

	JButton envoyer = new JButton("Envoyer");
	JTextField message = new JTextField("                                                                        ");

	public PanneauClient() {

		super();
		this.setTitle("Client - Panneau d'affichage");				// Titre de la fenetre
		this.setSize(250,120);										// Taille fenetre
		this.setLocation(300,150);									// Zone d'affichage de la fenetre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Container pane = getContentPane();
		pane.setLayout(new FlowLayout());

		pane.add(message, BorderLayout.NORTH);
		pane.add(envoyer, BorderLayout.SOUTH);

		envoyer.addActionListener((ActionListener)this);




		try {
			Socket monSocket;
			PrintWriter monPrintWriter;

			monSocket = new Socket("localhost", 8888);
			System.out.println("Client : " + monSocket);
			//monSocket.close();

			monPrintWriter = new PrintWriter(monSocket.getOutputStream());
			System.out.println("Envoie du message : Hello world");
			monPrintWriter.println("Hello World");
			monPrintWriter.flush();
			
			
			monSocket.close();
		}

		catch (Exception e) {
			System.out.println("Erreur cr�ation socket");
			e.printStackTrace();
		}

		System.out.println("La fenetre a ete creee !");
		this.setVisible(true);
	}

	public String emmetre() {
		return "Envoi d'un message";
	}


	public void actionPerformed(ActionEvent e) { 
		System.out.println(emmetre());
	}

	public static void main(String[] args) {

		PanneauClient fenetre = new PanneauClient();


	}

}