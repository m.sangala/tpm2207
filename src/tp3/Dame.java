package tp3;

public class Dame extends Humain {

	// Attribut
	private boolean libre;
	String liberte;

	// Contructeurs
	public Dame (String n) {
		super(n);
		Boisson ="Martini";
		libre = true;
		if (libre = true) { // Boucle permettant d'afficher un message en fonctions de l'attribut libre de type boolean
			liberte = " libre;";
		}
		else {
			liberte = " un otage";
		}
	}
	
	// Methodes
	public void priseEnOtage() { // Permet la prise d'otage
		libre = false;
		parler(" sAu secours ! " + "( " + Nom + " a �t� capturer" + " )");
	}
	
	public void estLiberee() { // Action lorsque la Dame est liber�
		libre = true;
		parler("Merci Cowboy. " + "( " + Nom + " a �t� liber�" + ") ");
	}
	
	public String quelEstTonNom() { // Donne son Nom
		return "Miss " + Nom;
	}
	
	public void sePresenter(){ // Permet a la dame de se presenter et de donner sa situation
		super.sePresenter();
		parler("Actuellement, je suis " + liberte); // Ce qui permet de donner sa situation
	}

}
