package tp3;

public class Brigand extends Humain{
	
	// Attributs
	private String look;
	private int nbDames, recompense;
	private boolean prison;
	
	// Constructeur
	public Brigand(String n) { // Fixe les caracteristique d'un brigand
		super(n);
		look = "mechant";
		prison = false;
		recompense = 100;
		nbDames = 0;
		Boisson = "cognac";
		
	}
	
	// Methodes
	public int getRecompense() { // Permet de fixer une recompense
		return recompense;
	}
	
	public String quelEstTonNom() { // Donne le Nom du brigand et son look
		return Nom + " le " + look;
	}
	
	public void sePresenter() { // Permet au brigand de se presenter
		super.sePresenter();
		parler("J'ai l'air " + look + " et j'ai enlev� " + nbDames + " dames");
		parler("Ma tete est mise a prix " + recompense + "$ !!");
	}
	
	public void enleve(Dame dame) { // Permet d'enlever un objet de class Dame
		nbDames = nbDames+1;
		recompense = recompense + 100;
		parler(quelEstTonNom() + " : Ah ah ! " + dame.quelEsttonNom() + ", tu es ma prisonniere !");
		dame.priseEnOtage();
	}
	
	public int nbDames() { // Affiche le nombre de dame attrap�
		return nbDames;
	}
	
	public void emprisonner (Sheriff s) { // Permet d'animer l'emprisonnement du brigand
		parler(quelEstTonNom() + " : Je suis fait ! " + s.quelEstTonNom() + " , tu m'as eu !");
	}

	

}
