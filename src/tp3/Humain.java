package tp3;

public class Humain {
	
	// Attribut
	protected String Nom, Boisson;

	// Constructeurs
	public Humain(String Nom) {
		this.Nom = Nom;
		Boisson = "lait";
	}
	
	// M�thodes
	String quelEsttonNom() { // Permet de donner son Nom
		return " Mon nom est " + Nom;
	}
	
	String quelEsttaBoisson() { // Permet de donner sa boisson
		return " ma boisson est " + Boisson;
	}
	
	void parler(String texte) { // Permet de ne plus repeter le m�me schema a chaque interaction
		System.out.println("(" + Nom + ") - " + texte);
	}
	
	public void sePresenter() { // Methode qui permet de presenter automatiquement l'humain
		parler(" Bonjour, je suis " + quelEsttonNom() + " et ma boisson preferee est le " + quelEsttaBoisson());
	}
	
	public void boire(){ // Permet de realiser l'action boire
		parler(" Ah ! Un bon verre de " + quelEsttaBoisson() + " !" + " GLOUPS !");
	}
}
