package tp3;

public class Cowboy extends Humain {
	
	// Attributs
	private int popularite;
	private String caracteristique;
	
	// Constructeur
	public Cowboy(String n) { // Constructeur ui fixe la popularit�, la boisson et la caracteristique du personnage
		super(n);
		popularite = 0;
		Boisson = "whiskey";
		caracteristique = "vaillant";
	}
	
	// Methodes
	public void tire(Brigand brigand) { //Permet de faire tir�e le Cowboy
		parler("Le " + caracteristique + " " + quelEsttonNom() + " tire sur " + brigand.quelEstTonNom() + " . PAN !");
		parler(quelEsttonNom() + " : Prend a, voyou !");
	}
	
	public void libere(Dame dame) { // Permet de liber� une dame
		dame.estLiberee();
		popularite = popularite + 10; // Prend en popularit�
		
	}


}
