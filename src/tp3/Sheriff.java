package tp3;

public class Sheriff extends Cowboy {
	
	// Attribut
	private int nbBrigand;
	
	// Constructeur
	public Sheriff (String n) { // Fixe le nombre de Brigand
		super(n);
		nbBrigand = 0;
	}
	
	// Methodes
	public String quelEstTonNom() { // Permet de donner le noom du Cowboy
		return "Sherif " + Nom;
		
	}
	
	public void sePresenter() { // Permet de presenter le nombre de brigand captur�
		super.sePresenter();
		parler("Pour le moment, j'ai capture " + nbBrigand + " brigand(s).");
	}
	
	public void coffrer(Brigand b) { // Permet d'attraper ( coffrer ) un brigand
		nbBrigand = nbBrigand + 1;
		parler(quelEstTonNom() + " : Au nom de la loi, je vous arrete, " + b.quelEstTonNom() + " !");
	}

}
