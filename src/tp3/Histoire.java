package tp3;

public class Histoire {

	public static void main(String[] args) {
		
		// Creation du personnage BOB
		Humain Bob;
		Bob = new Humain("Bob");
		Bob.sePresenter(); // Presentation de Bob
		Bob.boire(); // Bob bois
		
		// Creation du personnage de type Dame Maria
		Dame dame1 = new Dame("Lucie");
		dame1.sePresenter(); // Maria se presente et donne sa situation
		dame1.priseEnOtage(); // Maria se fait enlever
		dame1.estLiberee(); // Maria est lib�r�
		
		// Creation du personnage de type brigand appel� Gunther
		Brigand brigand1 = new Brigand("Gunther"); // Attribution du Nom
		brigand1.sePresenter();
		
		// Creation d'un personnage de type Cowboy
		Cowboy cowboy1 = new Cowboy("Juan");
		cowboy1.sePresenter();
				
		// liste d'action entre dame, brigand et cowboy
		dame1.sePresenter();
		brigand1.sePresenter();
		brigand1.enleve(dame1);
		brigand1.sePresenter();
		dame1.sePresenter();
		cowboy1.tire(brigand1);
		dame1.estLiberee();
		dame1.sePresenter();
		
		// Creation d'un personnage de type Sheriff
		Sheriff sherif1 = new Sheriff("Matthieu");
		sherif1.sePresenter();
		
		// Liste d'action entre brigand et sheriff
		sherif1.sePresenter();
		sherif1.coffrer(brigand1);
		brigand1.emprisonner(sherif1);
		sherif1.sePresenter();
		
		// Creation d'un personnage de type Sheriff d�riv� de Cowboy appel� Clint
		Cowboy clint = new Sheriff("Clint");
		clint.sePresenter();
		
		}

}
